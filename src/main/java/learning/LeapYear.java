package learning;

import java.util.Map;
import java.util.HashMap;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import static spark.Spark.*;

/**
 * Determines if a particular year is a leap year.
 * @author S. Hall on 3/2/2016.
 */
public class LeapYear {

    public static void main(String[] args) {
        String layout = "templates/layout.vm";

        get("/", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("template", "templates/home.vm");
            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());

        get("/detector", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("template", "templates/detector.vm");

            String year = request.queryParams("year");
            Integer integerYear = Integer.parseInt(year);

            Boolean isLeapYear = isLeapYear(integerYear);

            model.put("isLeapYear", isLeapYear);
            model.put("year", request.queryParams("year"));

            return new ModelAndView(model, layout);
        }, new VelocityTemplateEngine());
    }

    public static boolean isLeapYear(int year) {

        if (year % 400 == 0) {
            return true;
        } else if (year % 100 == 0) {
            return false;
        } else {
            return year % 4 == 0;
        }

    }

}
