package learning;

import java.util.ArrayList;

/**
 * FizzBuzz, but with PingPong instead!
 * <p>
 * <pre>
 *     Numbers divisible by 3 are replaced with "ping"
 *     Numbers divisible by 5 are replaced with "pong"
 *     Numbers divisible by 15 are replaced with "learning"
 * </pre>
 *
 * @author S. Hall on 3/2/2016.
 */
public class PingPong {

    public ArrayList pingPong(int range) {

        ArrayList<Object> arrayList = new ArrayList<>();

        for (int i = 1; i <= range; i++) {

            if (i % 15 == 0) {
                arrayList.add(i - 1, "pingpong");
            } else if (i % 5 == 0) {
                arrayList.add(i - 1, "pong");
            } else if (i % 3 == 0) {
                arrayList.add(i - 1, "ping");
            } else {
                arrayList.add(i - 1, i);
            }
        }

        return arrayList;
    }

}
