package learning;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * @author S. Hall on 3/2/2016.
 */
public class PingPongTest {

    @Test
    public void testPingPong() throws Exception {
        ArrayList<Object> expected = new ArrayList<>();

        // [1, 2, "ping", 4, "pong", "ping", 7]
        expected.add(1);
        expected.add(2);
        expected.add("ping");
        expected.add(4);
        expected.add("pong");
        expected.add("ping");
        expected.add(7);


        PingPong pingPongInstance = new PingPong();
        assertEquals(expected, pingPongInstance.pingPong(7));
    }
}