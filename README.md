# Learning Excercises
This isn't intended to be useful to anyone else, just a backup really. Inside
you'll find (eventually):

- Unit testing!
- Integration testing!
- Things that work (maybe, that's a stretch goal)!

## Libraries used/abused
- [Spark Framework](http://sparkjava.com/): A tiny Java web framework
- [jUnit](http://junit.org/): Unit testing!
- [Apache Velocity](http://velocity.apache.org/): Template system